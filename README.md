# Graham - June 2, 2020

## Installation

To run the application locally, complete the following steps:

1. `yarn`
2. `yarn start`
3. In a different tab, start up the server:
    1. Make sure you have the service account json for uploading to GCS in the root of the repo - this file should have been sent to you separately
    1. `export GOOGLE_APPLICATION_CREDENTIALS="/path/to/svc-account.json"`
    2. `yarn start api`

Run `yarn test file-manager` to run tests

You'll need Node 12 for the Express server to not throw an error when it boots up.

## Security

### Addressed

Malicious input
* Malicious inputs via text are handled by not trusting input that could come from the user, i.e. not using any of the
"unsafe" methods to put the content from the API into the DOM. `<script>alert('hi')</script>` or whatever else can be
used as input either in file names or in the search box without issue.

Issues relating to file uploads
* Files are uploaded to Google Cloud Storage. This accomplishes a few things, security-wise: we keep the files at arms
length - they aren't on our server; we won't be overrun by files that are too big or that are served too often, we let
Google handle that (or your CDN of choice could also be used!)
* Files are checked for their size (<10MB) on the server as well as in the frontend code before uploading them to GCS.
* Files are renamed with random strings, this helps to obfuscate how they're being stored (which doesn't matter that
much since we're using GCS but it would be very necessary when uploading to our own servers for example).
* The file extension isn't trusted, we use Jimp to detect the MIME type of the image and attach the correct extension
to the randomly generated string file name mentioned above. No regex is being used to detect the file extension as it
is ignored completely. If it isn't a supported file type an error is shown in the UI.
* The entire image file is read by Jimp and then rewritten to a new Buffer which is then uploaded to GCS.

Plentiful issues with Express, blocked by the use of [Helmet middleware](https://helmetjs.github.io/docs/)
* Clicking the link above will show you the list of all of the fixes it provides, we use the default + CSP
* However, all of these fixes won't be applied until the React site is served from the Express app

[Avoiding using POST](https://github.com/pillarjs/understanding-csrf#avoid-using-post)
* The api calls made from the frontend use PUT to upload a new file and DELETE to tell a file to be removed
* Not using POST means malicious third parties can't use an HTML form to submit data to these endpoints as a form can
only GET or POST.

Package versions in package.json are pinned
* The versions of installed packages won't be accidentally modified when a new minor version becomes available.

### Not addressed

Content-Security-Policy
* The biggest issue is that the app isn't deployed anywhere. If the app was being deployed it could serve the
React files out of its public assets, and default-src 'self' should be sufficient for those things to run successfully.
The other rule that would need to be added is `img-src https://storage.googleapis.com` so that images from GCS can be
rendered. Both of these rules are being applied in `apps/api/src/main.ts` as an example of what could be done. The app
could probably be renamed from api to something else that indicates it serves the assets as well as the api perhaps.

CSRF
* The files api call to Express could make Express put down an XSRF token in the cookies. This token would then be used
on the requests to the file put endpoint to ensure (as best we can) that the requests are being performed by a user of
the site and not a malicious third party.
* If the Express app served the React app it'd be simple to put an XSRF token in the initial header sent down to the
app, then it could be passed on each request to the backend.

## Improvements

Actual Database implementation
* Eventually it would make sense to have the metadata about the files you've uploaded persist somewhere. The files
uploaded to GCS _do_ persist, however the tracking of them in Express goes away whenever you restart the Express server.

Service account JSON for Google Cloud
* When the application is deployed our build machine would make sure the json file was ready and that the environment
variable was set to the correct spot for the app to load it.

Animations
* It'd be nice to add some animations to the file cells

UI in general
* Could use some love
* It would be nice to show a preview of the image about to be uploaded!
* It would be nice if choosing a new image would wipe out the document name if there was an error already

File rewriting causes image sizes (in kb) to change
* It'd be better if the size wasn't changed at all as that's what a user would probably expect.

## Libraries

This app uses [Nx](https://nx.dev/react) which is a set of tools for developing monorepos. The monorepo tooling makes
it very easy to run both an Express server and a React frontend simultaneously. It also makes it trivial for the front
and back end to share data definitions such as the Document and ListDocumentResponse seen in the useFiles effect.

However, Nx does bring with it a fair number of (primarily) dev dependencies. For the most part they're reasonable
though.

### Dependencies

#### Frontend

axios - Used to make requests to the Express server. Small library that makes dealing with requests
more straightforward than something like fetch.

react + react-dom - Gotta have 'em!

bootstrap + react-bootstrap - Styling used in the application

#### Backend

express - The backend is an Express server so this is required for the app.

helmet - Provides protective middleware for Express applications. Does things like remove the "X-Powered-By" header,
sets STS requirements, and provides the CSP for the app.

cookie-parser - Express middleware for parsing cookies.

csurf - Express middleware for handling CSRF.

@google-cloud/storage - This app uses Google Cloud Storage to store uploaded files for security reasons. This package
makes uploading those files very simple. This package is only used from the Express server.

body-parser - Used in the Express server to parse request bodies, seemingly required to do so.

multer - Provides the ability to parse multipart encoded forms to Express.

crypto-random-string - Provides cryptographically strong random strings, used for renaming image files before upload.

Jimp - Used for re-writing image files before uploading them, as well as for verifying file format.

### Dev dependencies

[@nrwl/\*](https://nx.dev/react) - these libraries mostly power the schematics and other tooling for Nx. They aren't 
used much in this app but you could do things like generate new React components or Express routes and the like using
them.

[@testing-library/react](https://testing-library.com/docs/react-testing-library/intro) - Very useful testing library. It
encourages (and enables) testing your software the way your users use it. That means avoiding testing implementation
details as much as you can by using the DOM to test. It provides ways to click buttons and add text to inputs, for
example. You can then check your rendered DOM to see if it performed the updates you expect. See the 
[file-upload.spec.tsx](https://gitlab.com/ghostlander/file-manager/-/blob/master/apps/file-manager/src/app/components/file-upload/file-upload.spec.tsx) file for a good example of this technique.

@types/\* - Used for interacting with libraries using Typescript that aren't written in Typescript

\*eslint\* - Many different libs needed that plug in different eslint rules, provided by Nx

jest - For testing

prettier - For code formatting

ts-jest, ts-node, tslint, typescript - For using Typescript

## API

The API offers a RESTful implementation dealing with files. Discussed below are the endpoints. First, one important
interface to consider:

```typescript
export interface Document {
  id: string;
  name: string;
  fileName: string;
  // Number of bytes
  size: number;
  file?: File;
  url?: string;
}
```

The Document interface is how the front and back end talk about the files being passed around.

### GET /api/files

Parameters: 
* search - string - non-required query parameter. Will filter the files by name if it is included, i.e.
`/api/files?search=hello%20world`
Returns:
* Status: 200
* The array of Documents currently being tracked.

### PUT /api/files

This endpoint takes a multipart/form-data request - this is required to upload the blob data for the image.

Parameters:
* name - string - the display name of the image
* fileName - string - the actual filename of the image as uploaded
* size - number - the number of bytes that the image is, used mostly for display
* file - binary - the actual blob image

Returns:
* Status: 201
* newFile - Document - the metadata describing the new file

### DELETE /api/files/:fileId

Parameters:
* the :fileId must be replaced with the id of the image, like `/api/files/my-image.png`
Returns:
* Status: 204

## Other notes

I'm sure there are many many things that could be done better in this application, but I just wanted to
say that I had an absolute blast researching and implementing the features you see here.
