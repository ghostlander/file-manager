import * as express from 'express';
import * as multer from 'multer';
import { Document, ListDocumentResponse } from '@file-manager/api-interfaces';
import { uploadImage } from './storage';
import { files } from './hardcodedfiles';
import * as cryptoRandomString from 'crypto-random-string';
import { detectExtensionAndReWriteImage } from './images';
import { ImageFile } from './interfaces';

const router = express.Router();

router.route('/').get((req, res) => {
  const search = req.query.search as string;
  // List out the files
  const resp: ListDocumentResponse = {
    documents: files,
  };

  if (search) {
    resp.documents = resp.documents.filter(
      (file) => file.name.toLowerCase().indexOf(search.toLowerCase()) > -1
    );
  }
  res.send(resp);
});

router.route('/:fileId').delete((req, res) => {
  // delete a file
  const index = files.findIndex((file) => file.id === req.params.fileId);
  files.splice(index, 1);
  res.status(204).send();
});

const upload = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
});
router.use(upload.single('file'));

router.route('/').put(async (req, res) => {
  // I have no idea how else to fix this besides having multer use TS ... better? Or something
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const imageFile = (req as any).file as ImageFile;

  try {
    const {
      detectedExtension,
      rewrittenImage,
    } = await detectExtensionAndReWriteImage(imageFile);

    if (['png', 'jpeg'].indexOf(detectedExtension) === -1) {
      res.status(400).send({
        message: detectedExtension + ' - must be .png or .jpg',
      });
      return;
    }

    // Generate a new filename
    const fileName =
      cryptoRandomString({ length: 16 }) + '.' + detectedExtension;

    const imageUrl = await uploadImage(rewrittenImage, fileName);

    const newFile: Document = {
      id: fileName, // Filename is random and cryptographically strong so it will serve as a fine ID.
      name: req.body.name,
      fileName: fileName,
      size: rewrittenImage.length,
      url: imageUrl,
    };
    files.push(newFile);
    res.status(201).send({ newFile });
  } catch (error) {
    console.log(error);
    res.status(400).send({ error: error.message });
  }
});

export default router;
