/**
 * This would likely be stored in a database in a production scenario.
 */

import { Document } from '@file-manager/api-interfaces';

export const files: Document[] = [];
