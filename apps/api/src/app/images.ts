import * as Jimp from 'jimp';
import { ImageRewriteResult, ImageFile } from './interfaces';

export async function detectExtensionAndReWriteImage(
  file: ImageFile
): Promise<ImageRewriteResult> {
  try {
    let image = await Jimp.read(file.buffer);

    if (image.getMIME() === 'image/jpeg') {
      // If it's a jpeg don't use 100% quality, as that can balloon image sizes.
      image = image.quality(80);
    }
    const rewrittenImage = await image.getBufferAsync(image.getMIME());
    return {
      detectedExtension: image.getExtension(),
      rewrittenImage,
    };
  } catch (error) {
    return { detectedExtension: error.message || error, rewrittenImage: null };
  }
}
