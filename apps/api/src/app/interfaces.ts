export interface ImageFile {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: Buffer;
  size: number;
}

export interface ImageRewriteResult {
  detectedExtension: string;
  rewrittenImage: Buffer;
}
