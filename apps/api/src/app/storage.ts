import { Storage } from '@google-cloud/storage';

const storage = new Storage();
const bucketName = 'image-uploader-storage-bucket';
export const bucket = storage.bucket(bucketName);

// Mostly copied from https://medium.com/@olamilekan001/image-upload-with-google-cloud-storage-and-node-js-a1cf9baa1876
const uploadImage = (fileBuffer, fileName): Promise<string> =>
  new Promise((resolve, reject) => {
    const buffer = fileBuffer;

    const blob = bucket.file(fileName);
    const blobStream = blob.createWriteStream({
      resumable: false,
    });

    blobStream
      .on('finish', () => {
        const publicUrl = `https://storage.googleapis.com/${bucket.name}/${fileName}`;
        resolve(publicUrl);
      })
      .on('error', (error) => {
        console.log(error);
        reject(`Unable to upload image, something went wrong`);
      })
      .end(buffer);
  });

export { uploadImage };
