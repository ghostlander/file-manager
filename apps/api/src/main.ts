import * as express from 'express';
import * as helmet from 'helmet';
// import * as csrf from 'csurf';
// import * as cookieParser from 'cookie-parser';

import filesRouter from './app/files';

const app = express();
app.use(helmet());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      imgSrc: ['https://storage.googleapis.com'],
    },
  })
);

// Potential way to get csrf onto endpoints without hosting the app using Express.
// const csrfProtection = csrf({ cookie: true });
// app.use(cookieParser());

// Apply the xsrf-token cookie to the GET /api/files request and the frontend can register
// that token for making future API calls.
// app.get('/api/files', (req, res) => {
// res.cookie('XSRF-TOKEN', 'hey');
// });

app.use('/api/files', filesRouter);

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log('Listening at http://localhost:' + port + '/api');
});
server.on('error', console.error);
