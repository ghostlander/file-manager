import React, { useState } from 'react';
import axios from 'axios';

import { Document } from '@file-manager/api-interfaces';

import AppLayout from './app-layout';
import { useFiles } from './effects/use-files';
import { useDebounce } from './effects/use-debounce';

export const AppContainer = () => {
  const [search, setSearch] = useState('');
  const [showFileDialog, setShowFileDialog] = useState(false);
  const [fileUploadError, setFileUploadError] = useState('');
  const debouncedSearch = useDebounce(search, 500);

  const [[loading, error, files], setFiles] = useFiles(debouncedSearch);

  const handleFileUpload = async (f: Document): Promise<boolean> => {
    if (!f || !f.size) {
      setShowFileDialog(false);
      return;
    }

    const formData = new FormData();
    formData.set('name', f.name);
    formData.set('fileName', f.fileName);
    formData.set('size', '' + f.size);
    formData.set('file', f.file);

    try {
      const response = await axios.put('/api/files', formData);
      // Append the new file locally so that we don't have to call api
      setFiles(fileState => {
        return [...fileState, { ...f, ...response.data.newFile }]
      });
      setShowFileDialog(false);
      setFileUploadError('');
      return true;
    } catch (error) {
      setFileUploadError(error.response.data.message);
      return false;
    }
  };

  const handleDelete = (id: string): void => {
    axios.delete(`/api/files/${id}`).then(() => {
      // Remove the file locally too so we don't have to call the api for the updated state
      // const newFiles = [...files].filter((file) => file.id !== id);
      setFiles(fileState => {
        return [...fileState].filter((file) => file.id !== id);
      });
    });
  };

  return (
    <AppLayout
      files={files}
      error={error}
      search={search}
      fileUploadError={fileUploadError}
      setSearch={setSearch}
      showFileDialog={showFileDialog}
      setShowFileDialog={setShowFileDialog}
      handleFileUpload={handleFileUpload}
      loading={loading}
      handleDelete={handleDelete}
    />
  );
};

export default AppContainer;
