import { render, fireEvent } from '@testing-library/react';

import React from 'react';
import AppLayout from './app-layout';

// jest.mock('axios');
// const axios = require('axios');

const mockFiles = [
  {
    id: 'faosjvcao1io1r1.png',
    name: 'vicarious',
    size: 19680,
    url:
      'some-url',
  },
  {
    id: 'f12oz731joijzv.jpg',
    name: 'party',
    size: 663227,
    url:
      'some-url2',
  },
  {
    id: '290j098jw0zdsfoivz.png',
    name: 'speaking',
    size: 564339,
    url:
      'some-url3',
  },
];

let allProps;

describe('AppLayout', () => {
  beforeEach(() => {
    allProps = {
      files: mockFiles,
      error: '',
      search: '',
      setSearch: jest.fn(),
      showFileDialog: false,
      setShowFileDialog: jest.fn(),
      handleClose: jest.fn(),
      loading: false,
      handleDelete: jest.fn()
    };
  });

  it('should show three documents if three are provided', () => {
    const { getByRole, queryByText } = render(<AppLayout {...allProps} />);
    expect(getByRole('heading', { name: /Documents/i }).textContent).toEqual('3 Documents');
    expect(queryByText(/Loading/i)).toBeNull();
    expect(queryByText(/Error in request/i)).toBeNull();
  });

  it('should make new api call if search text is typed', () => {
    const { getByRole } = render(<AppLayout {...allProps} />);

    fireEvent.change(getByRole('textbox'), { target: { value: 'speaking' } });
    expect(allProps.setSearch).toHaveBeenCalledWith('speaking');
  });

  it('should remove a document if delete is clicked', () => {
    const { getAllByRole } = render(<AppLayout {...allProps} />);

    fireEvent.click(getAllByRole('button', { name: /Delete/i})[0]);
    expect(allProps.handleDelete).toHaveBeenCalledWith('faosjvcao1io1r1.png');
  });

  it('should open upload file modal if Upload is clicked', () => {
    const { getByRole } = render(<AppLayout {...allProps} />);
    fireEvent.click(getByRole('button', { name: /Upload/i} ));
    expect(allProps.setShowFileDialog).toHaveBeenCalledWith(true);
  });

  it('should show an error if an error occurred', () => {
    const { getByText } = render(<AppLayout {...allProps} error="Network error in request" />);
    expect(getByText(/Network error in request/i)).not.toBeNull();
  });

  it('should show a loading message if api is loading', () => {
    const { getByText } = render(<AppLayout {...allProps} loading={true} />);
    expect(getByText(/Loading/i)).not.toBeNull();
  });
});
