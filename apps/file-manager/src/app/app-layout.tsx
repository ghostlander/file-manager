import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

import FileUpload from './components/file-upload/file-upload';
import FileViewer from './components/file-viewer/file-viewer';

import { Document } from '@file-manager/api-interfaces';
import FileSize from './components/file-size/file-size';
import { FileSearch } from './components/file-search/file-search';

interface AppLayoutProps {
  files: Document[];
  error: string;
  search: string;
  fileUploadError: string;
  setSearch: (search: string) => void;
  showFileDialog: boolean;
  setShowFileDialog: (show: boolean) => void;
  handleFileUpload: (file: Document) => Promise<boolean>;
  loading: boolean;
  handleDelete: (id: string) => void;
}

export const AppLayout = ({
  files,
  error,
  search,
  fileUploadError,
  setSearch,
  showFileDialog,
  setShowFileDialog,
  handleFileUpload,
  loading,
  handleDelete,
}: AppLayoutProps) => {
  return (
    <Container className="app">
      <Row>
        <Col md={6}>
          <FileSearch
            search={search}
            onChange={(newSearch) => setSearch(newSearch)}
          />
        </Col>
        <Col md={{ span: 3, offset: 3 }}>
          <Button block onClick={() => setShowFileDialog(true)}>
            Upload
          </Button>
          <FileUpload show={showFileDialog} handleFileUpload={handleFileUpload} fileUploadError={fileUploadError} />
        </Col>
      </Row>
      {loading || error ? (
        <Row>
          <Col>
            {loading && (
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {error && <div>Error in request: {error}</div>}
          </Col>
        </Row>
      ) : (
        <>
          <Row>
            <Col md={6}>
              <h2>{files.length} {files.length === 1 ? 'Document' : 'Documents' }</h2>
            </Col>
            <Col md={{ span: 3, offset: 3 }}>
              <FileSize files={files} />
            </Col>
          </Row>
          <Row>
            <FileViewer
              files={files}
              onDelete={handleDelete}
            />
          </Row>
        </>
      )}
    </Container>
  );
};

export default AppLayout;
