import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

import './app.scss';

import AppContainer from './app-container';

export const App = () => {
  return <AppContainer />
}

export default App;
