import React from 'react';

import './file-search.scss';

export const FileSearch = ({ search, onChange }) => {
  return (
    <input
      className="file-search"
      type="text"
      placeholder="Search documents..."
      value={search}
      onChange={(e) => onChange(e.target.value)}
    />
  );
}
