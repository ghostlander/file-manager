import React from 'react';
import { render } from '@testing-library/react';
import FileSize from './file-size';

describe('FileSize', () => {
  it('should show zero if no files passed in', () => {
    const { getByText } = render(<FileSize files={[]} />);
    expect(getByText(/Total size/i).textContent).toEqual('Total size: 0.0kb');
  });

  it('should sum up file sizes if files passed in', () => {
    const { getByText } = render(<FileSize files={[{size: 10000}, {size: 5000}]} />);
    expect(getByText(/Total size/i).textContent).toEqual('Total size: 15.0kb');
  });
});
