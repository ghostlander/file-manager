import React from 'react';

export const FileSize = ({ files }) => {
  return (
    <>
      Total size:{' '}
      {(files.reduce((prev, curr) => {
        prev += +curr.size;
        return prev;
      }, 0) / 1000).toFixed(1)}
      kb
    </>
  );
}

export default FileSize;
