import React from 'react';
import FileUpload from './file-upload';
import { render, fireEvent, wait, act } from '@testing-library/react';

import { getLotsOfTestData } from './validate-file.spec';

describe('File Upload', () => {
  it('should set info in the dom if within size requirements', async () => {
    const handleFileUpload = jest.fn();
    const { getByText, getByLabelText } = render(
      <FileUpload show={true} handleFileUpload={handleFileUpload} fileUploadError="" />
    );
    const file = new File(getLotsOfTestData(100), 'test.png', {
      type: 'image/png',
    });

    fireEvent.change(getByLabelText('Click browse'), { target: { files: [file] } });

    await wait(() => getByText('test.png'));
    expect(getByText('test.png')).not.toBeNull();
    expect(getByText(/Size of image/i).textContent).toEqual(
      'Size of image: 10.4kbNote that image size may change as part of the upload process.'
    );
  });

  it('should set error info in the dom if not right size or type', async () => {
    const handleFileUpload = jest.fn();
    const { getByText, getByLabelText, getByRole } = render(
      <FileUpload show={true} handleFileUpload={handleFileUpload} fileUploadError="" />
    );

    // Try a file with the wrong type
    const file = new File(['picture a test right here'], 'test.png', {
      type: 'application/octet-stream',
    });
    const imageInput = getByLabelText('Click browse');
    fireEvent.change(imageInput, { target: { files: [file] } });

    await wait(() => getByText('test.png'));
    expect(getByText('test.png')).not.toBeNull();
    expect(getByText(/Image is not valid/i)).not.toBeNull();
    expect(
      (getByRole('button', { name: 'Upload' }) as HTMLButtonElement).disabled
    ).toEqual(true);

    // Try a different, larger, file
    const tonsOfTestData = getLotsOfTestData(130000);
    const bigFile = new File([...tonsOfTestData], 'test.png', {
      type: 'image/png',
    });
    fireEvent.change(imageInput, { target: { files: [bigFile] } });
    expect(getByText('test.png')).not.toBeNull();
    expect(getByText(/Image is not valid/i).textContent).toEqual(
      'Image is not valid: too large: 13520.0kb'
    );
    expect(
      (getByRole('button', { name: 'Upload' }) as HTMLButtonElement).disabled
    ).toEqual(true);
  });
});
