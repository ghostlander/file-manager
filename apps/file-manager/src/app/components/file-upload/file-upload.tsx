import React, { useState } from 'react';
import { Document } from '@file-manager/api-interfaces';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';
import { validateFile } from './validate-file';

const defaultFileState = {
  size: 0,
  valid: false,
  file: null,
  fileName: '',
  errorReason: '',
};

interface FileUploadProps {
  show: boolean;
  handleFileUpload: (fileState?: Document) => Promise<boolean>;
  fileUploadError: string;
}

export default function FileUpload({
  show,
  handleFileUpload,
  fileUploadError,
}: FileUploadProps) {
  const [fileState, setFileState] = useState({ ...defaultFileState });
  const [fileName, setFileName] = useState('');
  const [uploading, setUploading] = useState(false);

  function handleFile(e: React.ChangeEvent): void {
    const target = e.target as HTMLInputElement;
    const { files } = target;
    if (files.length === 0) {
      // didn't upload a file?
      return;
    }
    const [valid, reason] = validateFile(files[0]);

    const newState = {
      file: files[0],
      size: files[0].size,
      fileName: files[0].name,
      valid,
      errorReason: reason,
    };

    if (fileName === '') {
      setFileName(files[0].name);
    }
    setFileState(newState);
  }

  async function handlePrimaryClick() {
    setUploading(true);
    const f: Document = {
      ...fileState,
      name: fileName,
    };
    const reset = await handleFileUpload(f);
    if (reset) {
      setFileState({ ...defaultFileState });
      setFileName('');
    }
    setUploading(false);
  }

  let fileInfoArea;
  if (fileState.size > 0 && fileState.valid) {
    fileInfoArea = (
      <div>
        Size of image: {(fileState.size / 1000).toFixed(1)}kb
        <br />
        Note that image size may change as part of the upload process.
      </div>
    );
  }
  if (fileState.errorReason !== '') {
    fileInfoArea = (
      <Alert variant="danger">
        Image is not valid: {fileState.errorReason}
      </Alert>
    );
  }
  if (fileUploadError !== '' && !uploading) {
    fileInfoArea = <Alert variant="danger">{fileUploadError}</Alert>;
  }
  if (uploading) {
    fileInfoArea = (
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    );
  }

  return (
    <>
      <Modal show={show} onHide={() => handleFileUpload()}>
        <Modal.Header closeButton>Upload a file</Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={(event) => {
              event.preventDefault();
              handlePrimaryClick();
            }}
          >
            <Form.Group controlId="name">
              <Form.Label>Document name</Form.Label>
              <Form.Control
                type="string"
                value={fileName}
                onChange={(event) => setFileName(event.target.value)}
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="document">
              <Form.File id="document" label="Upload a file" custom>
                <Form.File.Input
                  onChange={handleFile}
                  accept=".jpg, .jpeg, .png"
                />
                <Form.File.Label data-browse="Browse">
                  {fileState.fileName ? fileState.fileName : 'Click browse'}
                </Form.File.Label>
              </Form.File>
            </Form.Group>
          </Form>
          {fileInfoArea}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => handleFileUpload()}>
            Close
          </Button>
          <Button
            variant="primary"
            disabled={!fileState.valid}
            onClick={handlePrimaryClick}
          >
            Upload
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
