import { validateFile } from './validate-file';

describe('validateFile', () => {
  it('should return false if file is too large', () => {
    const lotsOfData = getLotsOfTestData(130000);
    const bigFile = new File([...lotsOfData], 'big.png', {
      type: 'image/png',
    });

    const [valid, reason] = validateFile(bigFile);
    expect(valid).toBe(false);
    expect(reason).toEqual('too large: 13520.0kb');
  });

  it('should return false if file is not correct type', () => {
    const file = new File(['just a test'], 'test.png', {
      type: 'application/octet-stream',
    });
    const [valid, reason] = validateFile(file);
    expect(valid).toBe(false);
    expect(reason).toEqual(
      'not a valid file type: application/octet-stream, need .png or .jpg'
    );
  });

  it('should return true with no reason if file is passes checks', () => {
    const file = new File(['a passing test 😌🤴🏼'], 'test.png', {
      type: 'image/png',
    });
    const [valid, reason] = validateFile(file);
    expect(valid).toBe(true);
    expect(reason).toEqual('');
  });
});

// Ridiculously arbitrary function to make a bunch of data for testing
// TODO: It feels odd to have this file be exported from here. Maybe a "test helpers" file in this folder makes
// more sense. This is the one time I have missed a __tests__ directory though
export function getLotsOfTestData(amount: number): string[] {
  const data = [];
  for (let i = 0; i < amount; i++) {
    data.push(
      'testdatatestdatatestdatatestdatatestdatatestdatatestdatatestdatatestdatatestdatatestdatatestdatatestdata'
    );
  }
  return data;
}
