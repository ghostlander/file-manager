// Validates files to our standards: < 10 MB, png or jpg/jpeg only.
// Returns a tuple: a boolean to indicate if it passed or failed, and a string for a reason as to why it failed.
export const validateFile = (file: File): [boolean, string] => {
  if (file.size > 10 * 1024 * 1024) {
    return [false, `too large: ${(file.size / 1000).toFixed(1)}kb`];
  }
  if (['image/png', 'image/jpeg'].indexOf(file.type) === -1) {
    return [false, `not a valid file type: ${file.type}, need .png or .jpg`];
  }
  return [true, ''];
};
