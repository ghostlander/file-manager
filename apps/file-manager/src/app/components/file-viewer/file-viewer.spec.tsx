import { render } from '@testing-library/react';
import React from 'react';
import FileViewer from './file-viewer';
import { Document } from '@file-manager/api-interfaces';

const mockFiles: Document[] = [
  {
    id: 'faosjvcao1io1r1.png',
    fileName: 'faosjvcao1io1r1.png',
    name: 'vicarious',
    size: 19680,
    url:
      'some-url',
  },
  {
    id: 'f12oz731joijzv.jpg',
    fileName: 'f12oz731joijzv.jpg',
    name: 'party',
    size: 663227,
    url:
      'some-url2',
  },
  {
    id: '290j098jw0zdsfoivz.png',
    fileName: '290j098jw0zdsfoivz.png',
    name: 'speaking',
    size: 564339,
    url:
      'some-url3',
  },
];

describe('File Viewer', () => {
  it('should show a message if no files have been added', () => {
    const { getByText } = render(<FileViewer files={[]} onDelete={jest.fn()} />);
    expect(getByText(/No files yet/i)).not.toBeNull();
  });

  it('should show three files if three files passed in', () => {
    const { getAllByRole } = render(<FileViewer files={mockFiles} onDelete={jest.fn()} />);
    expect(getAllByRole('heading').length).toEqual(3);
  })
});
