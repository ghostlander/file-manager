import React from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';

import { Document } from '@file-manager/api-interfaces';

import './file-viewer.scss';

interface FileViewerProps {
  files: Document[];
  onDelete: (fileId: string) => void;
}

export default function FileViewer({ files, onDelete }: FileViewerProps) {
  if (!files || files.length === 0) {
    return <Col>No files yet...</Col>;
  }

  return (
    <>
      {files.map((file: Document) => {
        return (
          <Col md={4} key={file.id}>
            <div className="file-box">
              <h6 className="file-box__heading">{file.name}</h6>
              <div className="file-box__actions">
                <span>{(file.size / 1000).toFixed(1)}kb</span>
                <Button onClick={() => onDelete(file.id)}>Delete</Button>
              </div>
              <div className="file-box__image-box">
                <img
                  className="file-box__image"
                  src={file.url}
                  alt={file.name}
                />
              </div>
            </div>
          </Col>
        );
      })}
    </>
  );
}
