import React, { useState } from 'react';
import { useFiles } from './use-files';
import { render, act, fireEvent } from '@testing-library/react';

jest.mock('axios');
const axios = require('axios');

// Testing component to wire-up the useFiles custom hook
const UseFilesTest = () => {
  const [search, setSearch] = useState('');
  const [[loading, error, files], setFiles] = useFiles(search);
  return (
    <>
    {!!loading && 'Loading!'}
    {!!error && `Error: ${error}`}
    {files.length > 0 &&
    <div>Files: {files.map((file, index) => <div key={index}>{file}</div>)}</div>}
    <input type="text" value={search} onChange={e => setSearch(e.target.value)} />
    </>
  )
}

describe('useFiles effect', () => {
  it('should set loading when making a request', async () => {
    const promise = Promise.resolve({ data: { documents: [] } });
    axios.get.mockImplementation(() => promise);
    const { getByText } = render(<UseFilesTest />);
    expect(getByText('Loading!')).not.toBeNull();
    await act(() => promise);
  });

  it('should set error if initial request errored out', () => {
    axios.get.mockImplementation(() => {throw new Error('api error')});
    const { getByText } = render(<UseFilesTest />);
    expect(getByText(/Error/).textContent).toEqual('Error: api error');
  });

  it('should set files if everything went through correctly', async () => {
    const promise = Promise.resolve({ data: { documents: ['one', 'two', 'three'] } });
    axios.get.mockImplementation(() => promise);
    const { getByText } = render(<UseFilesTest />);
    await act(() => promise);
    expect(getByText(/Files/i).textContent).toEqual('Files: onetwothree');
  });

  // TODO: Is there a better way to do this than this awaiting acting on promises?
  it('should make a new api request if search text is added', async () => {
    const promise = Promise.resolve({ data: { documents: ['one', 'two', 'three'] } });
    axios.get.mockImplementation(() => promise);
    const { getByRole } = render(<UseFilesTest />);
    await act(() => promise);

    const promise2 = Promise.resolve({ data: { documents: ['one', 'two', 'three'] } });
    axios.get.mockImplementation(() => promise2);
    // Put some text into the textbox to change the search value
    fireEvent.change(getByRole('textbox'), { target: { value: 'test' }});
    await act(() => promise2);
    expect(axios.get).toHaveBeenCalledWith('/api/files?search=test');
  })
});
