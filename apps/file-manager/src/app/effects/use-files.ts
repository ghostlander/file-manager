import { useState, useEffect, useReducer } from 'react';
import axios from 'axios';

import { Document, ListDocumentResponse } from '@file-manager/api-interfaces';

interface State {
  files: Document[];
  error: string;
  loading: boolean;
}

type Actions =
  | { type: 'error'; payload: string }
  | { type: 'loading' }
  | { type: 'set-documents'; payload: Document[] };

function reducer(state: State, action: Actions): State {
  switch (action.type) {
    case 'error':
      return { files: [], loading: false, error: action.payload };
    case 'loading':
      return { ...state, loading: true };
    case 'set-documents':
      return { files: action.payload, loading: false, error: '' };
  }
}

// useFiles is a custom hook for interacting with the files get/list endpoint.
// if search is !== '' it will be appended as a search term to the api.
// loading and error states are returned from this hook for use in the UI.
export function useFiles(
  search: string
): [
  [boolean, string, Document[]],
  (value: React.SetStateAction<Document[]>) => void
] {
  const [state, dispatch] = useReducer(reducer, {
    files: [],
    error: '',
    loading: false,
  });

  useEffect(() => {
    dispatch({ type: 'loading' });
    let url = '/api/files';
    if (search !== '') {
      url += `?search=${search}`;
    }
    const getAndSetDocuments = async () => {
      try {
        const response = await axios.get<ListDocumentResponse>(url);
        dispatch({ type: 'set-documents', payload: response.data.documents });
      } catch (error) {
        dispatch({ type: 'error', payload: error.message });
      }
    };
    getAndSetDocuments();
  }, [search]);

  return [
    [state.loading, state.error, state.files],
    (fileFn: (files: Document[]) => Document[]) => {
      dispatch({ type: 'set-documents', payload: fileFn(state.files) });
    },
  ];
}
