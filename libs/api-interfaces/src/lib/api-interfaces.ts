export interface Document {
  id?: string;
  name: string;
  fileName: string;
  // Number of bytes
  size: number;
  file?: File;
  url?: string;
}

export interface ListDocumentResponse {
  documents: Document[];
}
